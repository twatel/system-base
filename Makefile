##
## —————————————— MAKEFILE ————————————————————————————————————————————————————————
##
SHELL=/bin/bash

.DEFAULT_GOAL = help

.PHONY: help
help: ## Display help
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | sed -e 's/Makefile://' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-22s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


.PHONY: header
header: ## Display some informations about local machine
	@echo "*********************************** MAKEFILE ***********************************"
	@echo "HOSTNAME	`uname -n`"
	@echo "KERNEL RELEASE 	`uname -r`"
	@echo "KERNEL VERSION 	`uname -v`"
	@echo "PROCESSOR	`uname -m`"
	@echo "********************************************************************************"

##
## —————————————————————————— ENVIRONMENTS CONFIGURATION ——————————————————————————
##
.PHONY: env
env: header ## Prepare environment
	@[ -d "${PWD}/.direnv" ] || (echo "Venv not found: ${PWD}/.direnv" && exit 1)
	@pip3 install -U pip --no-cache-dir --quiet &&\
	echo -e "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} pip3" || \
	echo -e "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} pip3"

	@pip3 install -U wheel --no-cache-dir --quiet &&\
	echo -e "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} wheel" || \
	echo -e "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} wheel"

	@pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo -e "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} setuptools" || \
	echo -e "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} setuptools"

	@pip install -U --no-cache-dir -q -r requirements.txt &&\
	echo -e "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} PIP REQUIREMENTS" || \
	echo -e "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} PIP REQUIREMENTS"

.PHONY: prepare
prepare: header ## Install ansible-galaxy requirements
	@echo "***************************** ANSIBLE REQUIREMENTS *****************************"
	@ansible-galaxy install -fr ${PWD}/requirements.yml

##
## —————————————— CLEAN ———————————————————————————————————————————————————————————
##
.PHONY: clean
clean: ## Easy way to clean-up local environment
	@echo -e "${Green}Clean up environment${Color_Off}"
	@rm -rf .direnv

##
## —————————————— TESTS ———————————————————————————————————————————————————————————
##
.PHONY: tests-vbox
tests-vbox: header ## Test ansible role apply in vagrant vbox environment
	@echo -e "${Blue}Testing ansible role apply in vagrant vbox environment${Color_Off}"
	@cd ${TEST_VBOX_DIRECTORY} && vagrant up && vagrant provision
